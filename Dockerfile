# to do : WARNING: Running pip as the 'root' user can result in broken permissions and conflicting behaviour with the system # #package manager. It is recommended to use a virtual environment instead: https://pip.pypa.io/warnings/venv
FROM python:3.8
LABEL maintainer "Jose Dins Carlos Cabicho <jcabicho@gmail.com>"

WORKDIR /code

COPY requirements.txt /
RUN pip install -r /requirements.txt
COPY ./ ./

EXPOSE  30081 

#8050

CMD ["python", "./app.py"]
